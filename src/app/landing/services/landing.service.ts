import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment'
import { CookieService } from '../../common/cookie.service'

@Injectable({
  providedIn: 'root'
})
export class LandingService {

  constructor(private http: HttpClient, private cs: CookieService) { }

  loginUser(object) {
    return this.http.post(environment.BASE_URL + 'authenticateuser', object)
  }

  registerUser(object) {
    return this.http.post(environment.BASE_URL + 'signupuser', object)
  }

  checkLoginStatus() {
    if (this.cs.hasCookies()) return true
    else return false
  }

}
