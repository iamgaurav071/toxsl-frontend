import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { LandingService } from '../services/landing.service'
import { CookieService } from '../../common/cookie.service'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup

  emailError: boolean = false
  passwordError: boolean = false

  constructor(private fb: FormBuilder, private _ls: LandingService, private cs: CookieService, private toastr: ToastrService, private router: Router) { }

  ngOnInit(): void {
    this.initLoginForm()
  }

  initLoginForm() {
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  signInAttempt() {
    this.emailError = this.loginForm.controls.email.invalid
    this.passwordError = this.loginForm.controls.password.invalid

    if (!this.emailError && !this.passwordError) {
      let object = {
        email: this.loginForm.value.email,
        password: this.loginForm.value.password
      }
      this._ls.loginUser(object).subscribe((res: any) => {
        if (res.success) {
          if (this.cs.hasItem('id')) this.cs.removeItem('id', null, null);
          if (this.cs.hasItem('token')) this.cs.removeItem('token', null, null);
          if (this.cs.hasItem('email')) this.cs.removeItem('email', null, null);
          if (this.cs.hasItem('name')) this.cs.removeItem('name', null, null);

          this.cs.setItem('id', res.user.id, 24 * 3600, "/", null, null)
          this.cs.setItem('token', res.token, 24 * 3600, "/", null, null)
          this.cs.setItem('email', res.user.email, 24 * 3600, "/", null, null)
          this.cs.setItem('name', res.user.name, 24 * 3600, "/", null, null)

          this.toastr.success('Login', 'Successfully');
          this.router.navigate(['/users/'])
        } else {
          this.toastr.error("Invalid", "Email/Password");
        }
      })
    } else {
      console.log('validation trigger..')
    }
  }

}
