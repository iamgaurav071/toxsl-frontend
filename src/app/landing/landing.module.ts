import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'

import { LandingRoutingModule } from './landing-routing.module';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { InterceptorService } from './services/interceptor.service'

import { LandingService } from './services/landing.service';
import { CookieService } from '../common/cookie.service'



@NgModule({
  declarations: [LoginComponent, SignupComponent],
  imports: [
    CommonModule,HttpClientModule,
    LandingRoutingModule, ReactiveFormsModule, FormsModule
  ],
  providers: [LandingService, CookieService, {provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true}]
})
export class LandingModule { }
