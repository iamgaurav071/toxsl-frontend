import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

import { LandingService } from '../services/landing.service'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signUpForm: FormGroup
  nameError: boolean = false
  emailError: boolean = false
  passwordError: boolean = false

  constructor(private fb: FormBuilder, private _ls: LandingService, private router: Router,  private toastr: ToastrService) { }

  ngOnInit(): void {
    this.initForm()
  }

  initForm(){
    this.signUpForm = this.fb.group({
      name: ['', Validators.required],
      email:['', Validators.required],
      password: ['', Validators.required],
      cPassword: ['', Validators.required]
    })
  }

  registerUser(){
    this.nameError = this.signUpForm.controls.name.invalid
    this.emailError = this.signUpForm.controls.email.invalid
    this.passwordError = this.signUpForm.controls.password.invalid

    if (this.signUpForm.value.password != this.signUpForm.value.cPassword){
      this.toastr.warning("Password and confirm password aren't same")
      return false
    }

    if (!this.nameError && !this.emailError && !this.passwordError) {
      let object = {
        name: this.signUpForm.value.name,
        email: this.signUpForm.value.email,
        password: this.signUpForm.value.password
      }
      this._ls.registerUser(object).subscribe((res: any)=> {
        if (res.success){
          this.toastr.success("Registered successfully");
          this.router.navigate(['/'])
        } else {
          if (res.message == 'duplicate-email') this.toastr.warning("Email already exist")
          else this.toastr.error("Something went wrong")
        }
      })

    } else {
      console.log('validation trigger')
    }
  }

}
