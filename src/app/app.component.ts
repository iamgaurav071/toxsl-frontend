import { Component, OnInit } from '@angular/core';
import { CookieService } from './common/cookie.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'frontend';

  constructor(private cs: CookieService, private router: Router){}

  ngOnInit(){
    if (this.cs.hasCookies()) this.router.navigate(['/users'])
    else this.router.navigate(['/'])
  }
}
