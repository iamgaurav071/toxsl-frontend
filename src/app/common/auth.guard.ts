import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { LandingService } from '../landing/services/landing.service'

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private ls: LandingService){}

  canActivate(): boolean {
   if (this.ls.checkLoginStatus()) return true
   else return false
  }
  
}
