import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms'

import { UsersRoutingModule } from './users-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AddNotesComponent } from './add-notes/add-notes.component';
import { EditListComponent } from './edit-list/edit-list.component';
import { NotesListComponent } from './notes-list/notes-list.component';
import { UserService } from './services/user.service';
import { CookieService } from '../common/cookie.service';
import { InterceptorService } from './services/interceptor.service'
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthGuard } from '../common/auth.guard';
import { NgxSpinnerModule } from "ngx-spinner";

@NgModule({
  declarations: [HeaderComponent, FooterComponent, AddNotesComponent, EditListComponent, NotesListComponent],
  imports: [
    CommonModule,HttpClientModule,
    UsersRoutingModule, ReactiveFormsModule, FormsModule, NgxSpinnerModule
  ],
  providers: [UserService, CookieService, AuthGuard ,{provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true}]
})
export class UsersModule { }
