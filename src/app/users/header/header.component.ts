import { Component, OnInit } from '@angular/core';
import { CookieService } from '../../common/cookie.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private cs: CookieService) { }
  title: any
  ngOnInit(): void {
    this.title = this.cs.getItem('name')
  }

  logout(){
    this.cs.logout()
  }

}
