import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { UserService } from '../services/user.service'
import { ToastrService } from 'ngx-toastr';
import { CookieService } from '../../common/cookie.service'
import { HttpErrorResponse } from '@angular/common/http';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.css']
})
export class NotesListComponent implements OnInit {

  noteList: any
  constructor(private spinner: NgxSpinnerService,private us: UserService, private toastr: ToastrService, private router: Router, private cs: CookieService) { }

  ngOnInit(): void {
    this.getAllNotes()
  }

  getAllNotes(){
    this.spinner.show()
    this.us.getAllNotes().subscribe((res: any)=> {
      if (res.success) {
        this.noteList = res.data
        this.spinner.hide()
      } else {
        this.spinner.hide()
        this.toastr.error('Error', 'Something went wrong');
      }
    }, err => {
      if (err instanceof HttpErrorResponse) {
        this.toastr.error('Error', 'Something went wrong');
        if (err) {
          this.cs.logout();
        }
      }
    })
  }

  editNote(id){
    if (id) {
      this.router.navigate([`/users/edit-note/${id}`])
    }
  }

  deleteNote(id){
    if (id) {
      this.spinner.show()
      this.us.deleteNote(id).subscribe((res: any)=> {
        if (res.success) {
          this.getAllNotes()
          this.toastr.success('Deleted successfully');
          this.spinner.hide()
        } else {
          this.spinner.hide()
          this.toastr.error('Error', 'Something went wrong');
        }
      }, err => {
        if (err instanceof HttpErrorResponse) {
          this.toastr.error('Error', 'Something went wrong');
          if (err) {
            this.cs.logout();
          }
        }
      });
    }
  }

}
