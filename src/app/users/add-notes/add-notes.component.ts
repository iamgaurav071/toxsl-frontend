import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { UserService } from '../services/user.service'
import { Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { CookieService } from 'src/app/common/cookie.service';

@Component({
  selector: 'app-add-notes',
  templateUrl: './add-notes.component.html',
  styleUrls: ['./add-notes.component.css']
})
export class AddNotesComponent implements OnInit {

  listForm: FormGroup;

  titleError: boolean = false
  authorError: boolean = false
  noteError:boolean = false

  constructor(private cs: CookieService,private fb: FormBuilder, private _us: UserService, private router: Router, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.initForm()
  }

  initForm(){
    this.listForm = this.fb.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      note: ['', Validators.required]
    })
  }

  saveNote(){
    this.titleError = this.listForm.controls.title.invalid
    this.authorError = this.listForm.controls.author.invalid
    this.noteError = this.listForm.controls.note.invalid

    if (!this.titleError && !this.authorError && !this.noteError) {
      let object = {
        title: this.listForm.value.title,
        author: this.listForm.value.author,
        note: this.listForm.value.note
      }

      this._us.saveNote(object).subscribe((res: any)=> {
        if (res.success) {
          this.toastr.success('Saved', 'Successfully');
          this.router.navigate(['/users/'])
        } else {
          this.toastr.error('Error', 'Something went wrong');
        }
      }, err => {
        if (err instanceof HttpErrorResponse) {
          this.toastr.error('Error', 'Something went wrong');
          if (err) {
            this.cs.logout();
          }
        }
      })
    } else {
      console.log('Validation trigger')
    }
  }

}
