import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotesListComponent } from './notes-list/notes-list.component';
import { AddNotesComponent } from './add-notes/add-notes.component';
import { EditListComponent } from './edit-list/edit-list.component';
import { AuthGuard } from '../common/auth.guard'


const routes: Routes = [
  {path: '', component: NotesListComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'add-note', component: AddNotesComponent, pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'edit-note/:id', component: EditListComponent, pathMatch: 'full', canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
