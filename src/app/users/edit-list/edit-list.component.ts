import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service'
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { CookieService } from '../../common/cookie.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-edit-list',
  templateUrl: './edit-list.component.html',
  styleUrls: ['./edit-list.component.css']
})
export class EditListComponent implements OnInit {

  listId: any
  listForm: FormGroup;

  titleError: boolean = false
  authorError: boolean = false
  noteError:boolean = false

  constructor(private spinner: NgxSpinnerService, private cs: CookieService,private route: ActivatedRoute, private router: Router, private us: UserService, private fb: FormBuilder, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.initForm()
    this.route.params.subscribe((parms: any)=> {
      this.listId = parms['id']
      this.spinner.show()
      this.us.getListById(this.listId).subscribe((res: any)=> {
        if (res.success) {
          this.spinner.hide()
          this.listForm.patchValue({
            title: res.data.title,
            author: res.data.author,
            note: res.data.note
          })
        } else {
          this.spinner.hide()
          this.toastr.error('Error', 'Something went wrong');
          this.router.navigate(['/users/'])
        }
      }, err => {
        if (err instanceof HttpErrorResponse) {
          this.toastr.error('Error', 'Something went wrong');
          if (err) {
            this.cs.logout();
          }
        }
      })
    })
  }

  initForm(){
    this.listForm = this.fb.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      note: ['', Validators.required]
    })
  }

  updateNote(){
    this.titleError = this.listForm.controls.title.invalid
    this.authorError = this.listForm.controls.author.invalid
    this.noteError = this.listForm.controls.note.invalid

    if (!this.titleError && !this.authorError && !this.noteError) {
      this.spinner.show()
      let object = {
        id: this.listId,
        title: this.listForm.value.title,
        author: this.listForm.value.author,
        note: this.listForm.value.note
      }

      this.us.updateNote(object).subscribe((res: any)=> {
        this.spinner.hide()
        if (res.success) {
          this.toastr.success('Updated', 'Successfully');
          this.router.navigate(['/users/'])
        } else {
          this.spinner.hide()
          this.toastr.error('Error', 'Something went wrong');
        }
      }, err => {
        if (err instanceof HttpErrorResponse) {
          this.toastr.error('Error', 'Something went wrong');
          if (err) {
            this.cs.logout();
          }
        }
      })
    } else {
      console.log('Validation trigger')
    }
  }

}
