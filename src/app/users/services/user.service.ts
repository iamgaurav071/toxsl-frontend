import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  saveNote(obj){
    return this.http.post(environment.BASE_URL+'savenote', obj)
  }

  getAllNotes(){
    return this.http.get(environment.BASE_URL+'getallnotes')
  }

  deleteNote(id){
    return this.http.post(environment.BASE_URL+'deletenote', {id: id})
  }

  getListById(id){
    return this.http.post(environment.BASE_URL+'getlistbyid', {id: id})
  }

  updateNote(object){
    return this.http.post(environment.BASE_URL+'updatenote', object)
  }
}
