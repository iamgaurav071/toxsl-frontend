import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from '../../common/cookie.service'

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(private cs: CookieService) { }
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const req = {
      setHeaders: {
        'Content-Type': 'application/json',
        'Authorization': `${this.cs.getItem('token')}`,
        'userid': `${this.cs.getItem('id')}`
      }
    }
    request = request.clone(req)
    return next.handle(request)
  }
}
